package noughtsandcrosses.model;

public enum CellState {
    X,
    O,
    HINT,
    EMPTY
}
