package noughtsandcrosses.model.game;

import noughtsandcrosses.model.GamePoint;

import static noughtsandcrosses.model.game.BoardModel.*;

/**
 * Can peform analyse of board states and state whether a player has won and suggest a good move
 * The analyser is not 'turn aware' - it's suggestions will always be made on the assumption that the symbol queried is moving next
 */
public class BoardStateAnalyser {

    private enum OptionType {CLAIM_ANY, CLAIM_CORNER, CLAIM_CENTRE, BLOCK_TWO_STEP_WIN, TWO_STEP_WIN, BLOCK_TWO_STEP_DOUBLE, TWO_STEP_DOUBLE, BLOCK_WIN }

    private final String state;

    /**
     * State can't be changed - different states need a new object
     * @param state the current state of the board to analyse
     */
    public BoardStateAnalyser(String state) {
        this.state = state;
    }

    /**
     * Gets the next location the selected player should move
     * For the first move it will always suggest top left corner
     * Second move will be either the centre or the top left corner
     * Subsequent moves it will check for the following (in order):
     *    1. a winning move
     *    2. blocking an opponents winning move
     *    3. a move that sets up two winning options on their next turn
     *    4. a move that blocks the opponent from winning after two turns (prefering options that block moves that set up two options)
     *    5. a move that sets up one winning option on their next turn
     *    6. the centre square
     *    7. an open corner
     *    8. any other open space
     * If multiple positions exist in category 1, the hint will return the first position available, moving left-to-right, top-to-bottom
     * If multiple positions exist in a lower category AND no open positions exist in a higher category, the hint will return the
     * last position available
     * This can return null if there are no optimal positions - i.e. the game board is full; validate before or after calling this method
     * @param symbol the symbol to return the optimal location for
     * @param opponentSymbol the symbol of the opponent
     * @return a GamePoint identifying the optimal position or null if no optimal positions
     */
    public GamePoint getOptimalPosition(String symbol, String opponentSymbol) {

        GamePoint firstMove = tryFirstTwoMoves();   // first move logic is much simpler; return early if that's the case
        if (firstMove != null) return firstMove;

        GamePoint bestOption = null;
        OptionType bestOptionType = null;

        String[] stateArray = state.split("");

        for (int i = 0; i < stateArray.length; i++) {
            String position = stateArray[i];
            if (position.equals(PLAYER_ONE_SYMBOL) || position.equals(PLAYER_TWO_SYMBOL)) {
                continue;
            }

            GamePoint thisPoint = gamePointFromInternalPosition(i); // the position could be either H or a number so parseInt won't work

            if (isWinner(state.replace(position, symbol), symbol)) {
                return thisPoint;
            }

            OptionType thisOption = getBestNonWinningOption(position, i, symbol, opponentSymbol);

            bestOptionType = bestOptionType == null ? thisOption : thisOption.ordinal() > bestOptionType.ordinal() ? thisOption : bestOptionType;
            bestOption = bestOptionType == thisOption ? thisPoint : bestOption;
        }

        return bestOption;
    }

    /**
     * If it's the first or second move, return the appropriate hint option
     * Otherwise, returns null
     * The general logic doesn't work well for this case, so special logic is applied
     * @return suggested move for turn 1 or 2, otherwise null
     */
    private GamePoint tryFirstTwoMoves() {
        GamePoint topLeft = new GamePoint(0, 0);
        if (state.matches("[\\d" + HINT_SYMBOL + "]{9}")) { //first move
            return topLeft;
        }

        if (state.replace(PLAYER_ONE_SYMBOL, "").replace(PLAYER_TWO_SYMBOL, "").length() == 8) {
            if (state.charAt(4) == PLAYER_ONE_SYMBOL.charAt(0) || state.charAt(4) == PLAYER_TWO_SYMBOL.charAt(0)) {
                return topLeft;
            } else {
                return new GamePoint(1, 1);
            }
        }

        return null;
    }

    /**
     * Helper method for getOptimalPosition.
     * Assesses a given position and its index, and determines what type of move it would be
     * If a move meets multiple types, it follows the hierarchy in getOptimalMove
     * In certain circumstances it will 'look ahead' at the possible moves the opponent can make
     * If a TWO_STEP_WIN results in the opponent being able to both block the win option AND
     * set up their own TWO_STEP_DOUBLE option the option will not be returned as a TWO_STEP_WIN,
     * @param position position to move to
     * @param index the number of this position (required as the position can be either the number or HINT state)
     * @param symbol the symbol of the player who is about to move
     * @param opponentSymbol the symbol of the player who is not about to move
     * @return a MoveOption object representing the location and the category the option falls into
     */
    private OptionType getBestNonWinningOption(String position, int index, String symbol, String opponentSymbol) {

        String opponentMoves = state.replace(position, opponentSymbol); //used to test how bad it would be if the opponent went there (and we didn't)
        String playerMoves = state.replace(position, symbol);

        if (isWinner(opponentMoves, opponentSymbol)) { return OptionType.BLOCK_WIN; }

        OptionType forPlayer = null;
        switch (winningPositionsAvailable(playerMoves, symbol)) {
            case 2: // in theory (and with a cooperative player) you can get to a TWO_STEP_TRIPLE but, that move would also be a BLOCK_WIN, so we don't need to test for it
                return OptionType.TWO_STEP_DOUBLE;
            case 1:
                forPlayer = OptionType.TWO_STEP_WIN;    // we should only do this if we don't need to block a two-step-double
        }

        boolean isNotATrap = !isTrapMove(playerMoves, symbol, opponentSymbol);

        switch (winningPositionsAvailable(opponentMoves, opponentSymbol)) {
            case 2: // as per the above switch statement, no default case needed
                if (isNotATrap) { return OptionType.BLOCK_TWO_STEP_DOUBLE; }
            case 1:
                if (forPlayer == null) { return OptionType.BLOCK_TWO_STEP_WIN; }
        }

        if (forPlayer != null && isNotATrap) { return OptionType.TWO_STEP_WIN; }

        if (index == 4) { return OptionType.CLAIM_CENTRE; }

        if (index == 2 || index == 6 || index == 8) { return OptionType.CLAIM_CORNER; }

        return OptionType.CLAIM_ANY;
    }


    /**
     * Identifies whether TWO_STEP_WIN or BLOCK_TWO_STEP_DOUBLE move is actually trap;
     * by taking the move, the opponent can respond by setting up a double win of their own
     * (There are at least two board states that meet this circumstance)
     * @param state hypothetical board state after the potential move being evaluated is made
     * @param symbol the player whose move is being evaluated
     * @param opponentSymbol the opponent's move
     * @return true if move is a trap
     */
    private boolean isTrapMove(String state, String symbol, String opponentSymbol) {
        for (String position : state.split("")) {
            if (position.equals(PLAYER_ONE_SYMBOL) || position.equals(PLAYER_TWO_SYMBOL)) {
                continue;
            }

            String newState = state.replace(position, opponentSymbol);
            if (winningPositionsAvailable(newState, opponentSymbol) > 1 &&
                winningPositionsAvailable(newState, symbol) == 0) {
                return true;
            }
        }

        return false;
    }


    /**
     * Returns the number of winning positions available within one move for a given symbol and board state
     * @param symbol is the symbol of the player to test for winner
     * @return true if winning move is present, otherwise fault
     */
    private int winningPositionsAvailable(String state, String symbol) {
        int winCounter = 0;
        for (String position : state.split("")) {
            if (position.equals(PLAYER_ONE_SYMBOL) || position.equals(PLAYER_TWO_SYMBOL)) {
                continue;
            }

            if (isWinner(state.replace(position, symbol), symbol)) {
                winCounter++;
            }
        }

        return winCounter;
    }

    /**
     * Converts an internal position (i.e. the index of a character in the state String) to the corresponding GamePoint
     * @param internalPosition the internal position to convert
     * @return GamePoint the corresponding gamepoint
     */
    private GamePoint gamePointFromInternalPosition(int internalPosition) {
        return new GamePoint(internalPosition % 3, internalPosition / 3);
    }

    /**
     * Tests the current state to see if selected symbol is in a winning position
     * @param symbol String rep of the symbol who is being checked
     * @return true if the selected symbol has won
     */
    public Boolean isWinner(String symbol){
        return isWinner(state, symbol);
    }

    /**
     * Tests a given state to see if selected symbol has a winning position
     * @param state hypothetical state of the board
     * @param symbol who may have won
     * @return true if the selected symbol has won
     */
    private Boolean isWinner(String state, String symbol){
        if (state.matches(symbol.repeat(3) + "......")) { return true; }
        if (state.matches("..." + symbol.repeat(3) + "...")) { return true; }
        if (state.matches("......" + symbol.repeat(3))) { return true; }
        if (state.matches(symbol + "..." + symbol + "..." + symbol)) { return true; }
        if (state.matches("." + symbol + ".." + symbol + ".." + symbol + ".")) { return true; }
        if (state.matches(".." + symbol + "." + symbol + "." + symbol + "..")) { return true; }
        if (state.matches(symbol + ".." + symbol + ".." + symbol + "..")) { return true; }
        return state.matches(".." + symbol + ".." + symbol + ".." + symbol);
    }

}
