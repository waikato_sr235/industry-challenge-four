package noughtsandcrosses.model.game;

import noughtsandcrosses.model.GamePoint;

import java.util.Objects;

public class Move {

    private String player;

    private final GamePoint location;

    public Move(GamePoint location) {
        this.location = location;
    }

    public Move(String player, GamePoint location) {
        this.player = player;
        this.location = location;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public GamePoint getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return player + " claimed " + location.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return player.equals(move.player) &&
                location.equals(move.location);
    }
    @Override
    public int hashCode() {
        return Objects.hash(player, location);
    }

}
