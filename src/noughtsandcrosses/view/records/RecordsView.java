package noughtsandcrosses.view.records;

import noughtsandcrosses.model.records.Record;
import noughtsandcrosses.model.records.RecordSummary;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class RecordsView extends JFrame {

    public static boolean exists;

    private final Dimension SUMMARY_DIMENSIONS = new Dimension(480, 240);
    private final Dimension RECORD_DIMENSIONS = new Dimension(480, 360);

    private final List<Record> records;
    private final List<RecordSummary> summaryRecords;

    public RecordsView(List<Record> records, List<RecordSummary> summaryRecords) {
        this.records = records;
        this.summaryRecords = summaryRecords;
        exists = true;

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                RecordsView.exists = false;
            }
        });
    }

    public void createAndShowGUI() {
        Container visibleArea = getContentPane();
        visibleArea.setLayout(new BoxLayout(visibleArea, BoxLayout.PAGE_AXIS));

        JPanel summaryPanel = createSummaryPanel();
        summaryPanel.setPreferredSize(SUMMARY_DIMENSIONS);
        JPanel recordsPanel = createRecordsPanel();
        recordsPanel.setPreferredSize(RECORD_DIMENSIONS);

        visibleArea.add(summaryPanel);
        visibleArea.add(recordsPanel);

        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(screenSize.width / 2 - getWidth() / 2, screenSize.height / 2 - getHeight() / 2);

        setVisible(true);
    }

    /**
     * Creates the records panel
     * @return configured JPanel for the records table
     */
    private JPanel createRecordsPanel() {
        RecordTableAdapter recordTableAdapter = new RecordTableAdapter(records);
        return createDisplayPanel(recordTableAdapter, "Match-by-match results");
    }

    /**
     * Creates the summary records panel
     * @return configured JPanel for the summary records table
     */
    private JPanel createSummaryPanel() {
        RecordSummaryTableAdapter recordSummaryTableAdapter = new RecordSummaryTableAdapter(summaryRecords);
        return createDisplayPanel(recordSummaryTableAdapter, "Player-by-player summary of performance");
    }

    /**
     * Generic method to create a table within a Panel with a heading and scroll bars
     * @param tableModel model that the table should use
     * @param header heading to display above the table
     * @return a configured JPanel
     */
    private JPanel createDisplayPanel(AbstractTableModel tableModel, String header) {
        JPanel displayPanel = new JPanel();
        displayPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.PAGE_AXIS));

        JTable displayTable = new JTable(tableModel);
        JScrollPane tableContainer = new JScrollPane(displayTable);

        JLabel tableHeading = new JLabel(header);
        tableHeading.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        displayPanel.add(tableHeading);
        displayPanel.add(tableContainer);

        return displayPanel;
    }

}
