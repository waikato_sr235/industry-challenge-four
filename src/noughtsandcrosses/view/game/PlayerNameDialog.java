package noughtsandcrosses.view.game;

import noughtsandcrosses.model.game.BoardModel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * A modal dialog that prompts the players to enter their names, and select a computer opponent if playing alone
 */
public class PlayerNameDialog extends JDialog {
    private static final int BORDER_SIZE = 5;
    private static final int PREFERRED_HEIGHT = 40;
    private static final int PREFERRED_WIDTH = 420;
    private static final String PLAYER_ONE = "Player One";
    private static final String PLAYER_TWO = "Player Two";

    private final BoardModel model;
    private JTextField txtPlayerOne;
    private JTextField txtPlayerTwo;
    private JCheckBox isComputer;
    private JButton okayButton;

    private final List<ViewObserver> listeners;

    public PlayerNameDialog(BoardModel model) {
        super();
        this.model = model;
        listeners = new ArrayList<>();

        setTitle("Enter Player Names");
        setModal(true);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    }

    public void addListener(ViewObserver listener) {
        listeners.add(listener);
    }

    public void removeListener(ViewObserver listener) {
        listeners.remove(listener);
    }

    /**
     * Creates and shows a modal dialog prompting the player to provide their names or request a computer opponent
     * Passes the enter names back to the noughtsandcrosses.model
     */
    public void requestPlayerNames() {
        configureDialog(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(screenSize.width / 2 - getWidth() / 2, screenSize.height / 2 - getHeight() / 2);

        txtPlayerOne.getDocument().addDocumentListener(documentListener);
        txtPlayerOne.getDocument().putProperty("me", txtPlayerOne);

        txtPlayerTwo.getDocument().addDocumentListener(documentListener);
        txtPlayerTwo.getDocument().putProperty("me", txtPlayerTwo);

        isComputer.addActionListener(fieldListener);

        tryEnableOkayButton();
        setVisible(true);

        listeners.forEach(listener -> listener.setNames(txtPlayerOne.getText(), txtPlayerTwo.getText()));
    }

    /**
     * Configures dialog to show all required controls
     *
     * @param size Dimension object with the preferred width and height for each component with the dialog panel
     */
    private void configureDialog(Dimension size) {
        JPanel playerInputPanel = getInputPanel(size);
        this.add(playerInputPanel);
        this.pack();
    }

    /**
     * Creates the main panel the dialog will display
     *
     * @param size Dimension object with the preferred width and height for each component within the dialog panel
     * @return JPanel configure with all required components
     */
    private JPanel getInputPanel(Dimension size) {
        JPanel playerInputPanel = new JPanel();
        playerInputPanel.setLayout(new BoxLayout(playerInputPanel, BoxLayout.PAGE_AXIS));
        playerInputPanel.setAlignmentX(LEFT_ALIGNMENT);
        playerInputPanel.setBorder(BorderFactory.createEmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));

        JPanel isComputerPanel = getIsComputerPanel(size);      // getPlayerPanel will try to update isComputerPanel so this must be called first

        JPanel playerOnePanel = getPlayerPanel(size, PLAYER_ONE);
        JPanel playerTwoPanel = getPlayerPanel(size, PLAYER_TWO);
        JPanel buttonArea = getButtonArea(size);

        playerInputPanel.add(playerOnePanel);
        playerInputPanel.add(playerTwoPanel);
        playerInputPanel.add(isComputerPanel);
        playerInputPanel.add(buttonArea);
        return playerInputPanel;
    }

    /**
     * Returns a JPanel with a JButton
     *
     * @param size dimension object with preferred height and width
     * @return configured JPanel with a JButton object
     */
    private JPanel getButtonArea(Dimension size) {
        JPanel buttonArea = new JPanel();
        buttonArea.setPreferredSize(size);
        buttonArea.setAlignmentX(Component.CENTER_ALIGNMENT);

        okayButton = new JButton("Play!");
        buttonArea.add(okayButton);
        okayButton.setEnabled(false);
        okayButton.addActionListener(actionEvent -> this.setVisible(false));

        return buttonArea;
    }

    /**
     * Configures the panel showing the player area
     *
     * @param size      dimension object with the preferred height and width
     * @param forPlayer which player it is for - constants provided in class
     * @return configured JPanel with JTextField and JLabel
     */
    private JPanel getPlayerPanel(Dimension size, String forPlayer) {
        JPanel playerPanel = new JPanel();
        playerPanel.setPreferredSize(size);
        playerPanel.setBorder(BorderFactory.createEmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
        playerPanel.setAlignmentX(LEFT_ALIGNMENT);

        JLabel lblPlayerOne = new JLabel("Enter " + forPlayer + "'s Name");
        playerPanel.add(lblPlayerOne);

        if (forPlayer.equals(PLAYER_ONE)) {
            txtPlayerOne = initialiseJTextField(model.getPlayerOne());
            playerPanel.add(txtPlayerOne);
        } else {
            txtPlayerTwo = initialiseJTextField(model.getPlayerTwo());
            playerPanel.add(txtPlayerTwo);
        }

        return playerPanel;
    }

    /**
     * Initialises a JTextField for player name input
     * @param initialValue the starting value of the field (or null)
     * @return an initialised and configured JTextField
     */
    private JTextField initialiseJTextField(String initialValue) {
        JTextField result = new JTextField(32);
        result.setText(initialValue);


        if (result.getText().equals(BoardModel.COMPUTER)) {
            result.setEditable(false);
            isComputer.setSelected(true);
        }

        return result;
    }

    /**
     * Configures the panel that displays the computer opponent checkbox
     *
     * @param size dimension object with preferred height and width
     * @return JPanel with a checkbox
     */
    private JPanel getIsComputerPanel(Dimension size) {
        JPanel isComputerPanel = new JPanel();
        isComputerPanel.setPreferredSize(size);
        isComputerPanel.setBorder(BorderFactory.createEmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
        isComputerPanel.setAlignmentX(LEFT_ALIGNMENT);

        isComputer = new JCheckBox("Computer Opponent");
        isComputerPanel.add(isComputer);

        if (model.getPlayerTwo() != null) {
            isComputer.setSelected(model.getPlayerTwo().equals(BoardModel.COMPUTER));
        }

        return isComputerPanel;
    }

    /**
     * Checks relevant fields to see if okay button can be enabled
     */
    private void tryEnableOkayButton() {

        boolean bothPlayersName = txtPlayerOne.getText().length() > 0 && txtPlayerOne.getText().length() <= 32 &&
                                  txtPlayerTwo.getText().length() > 0 && txtPlayerOne.getText().length() <= 32;

        boolean onePlayersNameAndComputer = false;
        if (isComputer.isSelected()) {
            if (txtPlayerOne.getText().equals(BoardModel.COMPUTER)) {
                onePlayersNameAndComputer = txtPlayerTwo.getText().length() > 0 && txtPlayerOne.getText().length() <= 32;
            } else {
                onePlayersNameAndComputer = txtPlayerOne.getText().length() > 0 && txtPlayerOne.getText().length() <= 32;
            }
        }


        okayButton.setEnabled(bothPlayersName || onePlayersNameAndComputer);
    }

    /**
     * Listens to the text fields to see if the okay button can be enabled
     * Also ensures that input is valid - there can only be one computer player name
     * If the computer checkbox is selected, then the default behaviour is to set the second player to be the computer
     * If computer is manually entered, that player is the computer
     */
    private final DocumentListener documentListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            JTextField textField = getSource(documentEvent);
            if (textField == null) return;

            String currentValue = textField.getText();
            if (currentValue.equals(BoardModel.COMPUTER)) {
                if (isComputer.isSelected()) {
                    JOptionPane.showMessageDialog(null, "You may not have two computer players", "Invalid Player Name", JOptionPane.ERROR_MESSAGE);
                    okayButton.setEnabled(false);
                    return; // prevents the okayButton from being re-enabled

                } else {

                    if (textField.getText().equals(BoardModel.COMPUTER)) {
                        textField.setEditable(false);
                        isComputer.setSelected(true);
                    }
                }
            }

            if (currentValue.length() > 32) {
                JOptionPane.showMessageDialog(null, "Player names may not be more than 32 characters", "Player Name Too Long", JOptionPane.ERROR_MESSAGE);
            }

            tryEnableOkayButton();
        }

        private JTextField getSource(DocumentEvent documentEvent) {
            Object source = documentEvent.getDocument().getProperty("me");
            JTextField textField;

            if (source instanceof JTextField) {
                textField = ((JTextField) source);
            } else {
                return null;
            }
            return textField;
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            tryEnableOkayButton();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            // not relevant for this usage
        }
    };

    /**
     * Listens to the checkbox to see if the okay button can be enabled
     */
    private final ActionListener fieldListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (isComputer.isSelected()) {
                isComputer.setSelected(false);  // toggling this checkbox to avoid a false positive when the textField change event is fired below
                txtPlayerTwo.setText(BoardModel.COMPUTER);
                isComputer.setSelected(true);
                txtPlayerTwo.setEditable(false);

            } else {
                /*  needs to be separate if clauses to handle the situation where the user has already typed computer manually into the first field
                    then tried to type it manually into the second field as well
                */
                if (txtPlayerTwo.getText().equals(BoardModel.COMPUTER)) {
                    txtPlayerTwo.setText("");
                    txtPlayerTwo.setEditable(true);
                }

                if (txtPlayerOne.getText().equals(BoardModel.COMPUTER)) {
                    txtPlayerOne.setText("");
                    txtPlayerOne.setEditable(true);
                }

            }

            tryEnableOkayButton();
        }
    };
}
