package noughtsandcrosses.model.records;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRecord {

    private Record sut;

    @BeforeEach
    public void setUp() {
        sut = new Record("Player One", "Player Two", "Player One", LocalDate.of(2020, 5 ,31));
    }

    @Test
    public void testGetPlayerOne() {
        assertEquals("Player One", sut.getPlayerOne());
    }

    @Test
    public void testGetPlayerTwo() {
        assertEquals("Player Two", sut.getPlayerTwo());
    }

    @Test
    public void testGetResult() {
        assertEquals("Player One", sut.getResult());
    }

    @Test
    public void testGetDatePlayed() {
        assertEquals(LocalDate.of(2020, 5, 31), sut.getDatePlayed());
    }
}
