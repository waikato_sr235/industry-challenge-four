package noughtsandcrosses.model.records;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRecordSummary {

    private RecordSummary sut;

    @BeforeEach
    public void setUp() {
        sut = new RecordSummary("Player One", 10, 1, 9);
    }

    @Test
    public void testGetName() {
        assertEquals("Player One", sut.getName());
    }

    @Test
    public void testGetMatches() {
        assertEquals(20, sut.getMatches());
    }

    @Test
    public void testGetWins() {
        assertEquals(10, sut.getWins());
    }

    @Test
    public void testGetLosses() {
        assertEquals(1, sut.getLosses());
    }

    @Test
    public void testGetDraws() {
        assertEquals(9, sut.getDraws());
    }
}
