package noughtsandcrosses.view.game;

import org.junit.jupiter.api.Test;

/**
 * Abstract test class that should be extended by any class providing unit tests for an object implementing the ViewObserver interface
 */
public abstract class TestViewObserver {

    @Test
    public abstract void testMoveMade();

    @Test
    public abstract void testHintRequested();

    @Test
    public abstract void testPlayAgain();

    @Test
    public abstract void testRecordsRequested();

    @Test
    public abstract void testExit();
}
